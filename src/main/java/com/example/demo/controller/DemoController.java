package com.example.demo.controller;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("demo")
public class DemoController {
    @RequestMapping(value = "/greetings",method = RequestMethod.GET)
    public ResponseEntity<String> getGreetings(){
        return new ResponseEntity<>("Hey, Welcome to the K8S !!!!!.", HttpStatus.OK);
    }
}
